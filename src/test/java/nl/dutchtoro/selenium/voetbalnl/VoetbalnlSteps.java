package nl.dutchtoro.selenium.voetbalnl;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nl.dutchtoro.selenium.voetbalnl.config.SeleniumConfig;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Cucumber steps for voetbal.nl.
 */
@ContextConfiguration(classes = { SeleniumConfig.class })
public class VoetbalnlSteps {

	WebDriver dr;

	@Autowired( required = true )
	SeleniumConfig config;

	@Given("^navigate to voetbalnl page$")
	public void navigate() {
		System.setProperty("webdriver.chrome.driver", config.getEnv().getProperty("chromedriver"));
		ChromeOptions options = new ChromeOptions();
		options.addArguments("user-data-dir=C:/Users/windows_username/AppData/Local/Google/Chrome/User Data");
		options.addArguments("--start-maximized");
		dr = new ChromeDriver(options);
		dr.get(config.getEnv().getProperty("v-baseurl"));
	}

	@When("^user logged in using email and password from propertyfile$")
	public void fillLoginForm() throws InterruptedException {
		if (Boolean.parseBoolean(config.getEnv().getProperty("login"))) {
			System.out.println("log in");
			System.out.println(dr.findElement(By.xpath("//*[@id='edit-email']")).isDisplayed());
			dr.findElement(By.xpath("//*[@id='edit-email']")).sendKeys(config.getEnv().getProperty("email"));
			dr.findElement(By.xpath("//*[@id='edit-password']")).sendKeys(config.getEnv().getProperty("wachtwoord"));
			WebElement element = dr.findElement(By.id("edit-send"));
			((JavascriptExecutor)dr).executeScript("arguments[0].click();", element);
			dr.manage().timeouts().implicitlyWait(270, TimeUnit.SECONDS);
		} else {
			// already logged on (previous session)
			System.out.println("already logged in");
		}
		dr.findElement(By.xpath("//*[@class='dashboard-header-maintitle']"));
		System.out.println("Logged in");
		Set<String> allwh = null;
		Optional<String> firstTab = null;
		Optional<String> secondTab = null;
		// open second tab in firefox
		((JavascriptExecutor) dr).executeScript(
				"window.open('" + config.getEnv().getProperty("c-baseurl") + "?" +
						config.getEnv().getProperty("c-clubuitslag-url") + "');");
		allwh = dr.getWindowHandles();
		firstTab = allwh.stream().findFirst();
		secondTab = allwh.stream().skip(1).findFirst();
		dr.switchTo().window(firstTab.get());
		Thread.sleep(1000);


		if (Boolean.parseBoolean(config.getEnv().getProperty("clubuitslag"))) {
			// Get clubuitslagen
			String[] clubuitslagen = config.getEnv().getProperty("v-clubuitslag-url").split(";");
			boolean first = true;
			for (String clubuitslag : clubuitslagen) {
				dr.get(clubuitslag);
				String sourceHtml = dr.getPageSource();
				if (first) {
					// open second tab in firefox
					dr.switchTo().window(secondTab.get());
					dr.get(config.getEnv().getProperty("c-baseurl") + "?" +	config.getEnv().getProperty("c-clubuitslag-url"));
					first = false;
				} else {
					dr.switchTo().window(secondTab.get());
					dr.get(config.getEnv().getProperty("c-baseurl") + "?" +	config.getEnv().getProperty("c-clubuitslag-url"));
					if(dr.findElement(By.xpath("//input[@type='checkbox' and @name='cleanTable']")).isSelected()) {
					  dr.findElement(By.xpath("//input[@type='checkbox' and @name= 'cleanTable']")).click();
					}
				}
				dr.findElement(By.xpath("//*[@name='html']"));
				
				setClipboardContents(sourceHtml);
				dr.findElement(By.xpath("//*[@name='html']")).click();
				dr.findElement(By.xpath("//*[@name='html']")).sendKeys(Keys.CONTROL + "v");
				//dr.findElement(By.xpath("//*[@name='html']")).sendKeys(sourceHtml);
				dr.findElement(By.xpath("//*[@name='Submit']")).click();
				Assert.assertTrue(dr.getPageSource().contains("ClubUitslagen opgeslagen voor teamID: 0"));
				// switch to first tab
				dr.switchTo().window(firstTab.get());
				System.out.println("Window handle: " + firstTab.get());
			}
		} else {
			System.out.println("Clubuitslag niet doorvoeren");
		}

		if (Boolean.parseBoolean(config.getEnv().getProperty("clubprogramma"))) {
			// Get clubprogramma
			String[] clubprogramma = config.getEnv().getProperty("v-clubprogramma-url").split(";");
			boolean first = true;
			for (String clubprog : clubprogramma) {
				dr.get(clubprog);
				Thread.sleep(1000);
				dr.findElement(By.xpath("//span[@class='button--show-more-label']")).click();
				Thread.sleep(2000);
				String sourceHtml = dr.getPageSource();
				if (first) {
					// open second tab in firefox
					dr.switchTo().window(secondTab.get());
					dr.get(config.getEnv().getProperty("c-baseurl") + "?" +	config.getEnv().getProperty("c-clubprogramma-url"));
					first = false;
				} else {
					dr.switchTo().window(secondTab.get());
					dr.get(config.getEnv().getProperty("c-baseurl") + "?" +	config.getEnv().getProperty("c-clubprogramma-url"));
					if(dr.findElement(By.xpath("//input[@type='checkbox' and @name='cleanTable']")).isSelected()) {
					  dr.findElement(By.xpath("//input[@type='checkbox' and @name= 'cleanTable']")).click();
					}
				}
				dr.findElement(By.xpath("//*[@name='html']"));
				
				setClipboardContents(sourceHtml);
				dr.findElement(By.xpath("//*[@name='html']")).click();
				dr.findElement(By.xpath("//*[@name='html']")).sendKeys(Keys.CONTROL + "v");
				//dr.findElement(By.xpath("//*[@name='html']")).sendKeys(sourceHtml);
				dr.findElement(By.xpath("//*[@name='Submit']")).click();
				Assert.assertTrue(dr.getPageSource().contains("ClubProgramma opgeslagen voor teamID: 0"));
				// switch to first tab
				dr.switchTo().window(firstTab.get());
				System.out.println("Window handle: " + firstTab.get());
			}
		} else {
			System.out.println("Clubprogramma niet doorvoeren");
		}
		
		if (Boolean.parseBoolean(config.getEnv().getProperty("teamsdata"))) {
			// afhandeling per team: programma, uitslagen en stand
			String[] teams = config.getEnv().getProperty("teams").split(";");
			String sourceHtml = null;
			boolean skipUitslag = Boolean.parseBoolean(config.getEnv().getProperty("skipUitslag"));
			boolean skipProg = Boolean.parseBoolean(config.getEnv().getProperty("skipProgramma"));
			boolean skipStand = Boolean.parseBoolean(config.getEnv().getProperty("skipStand"));
			String[] skipUitslagTeam = config.getEnv().getProperty("skipUitslagTeam").split(";");
			String[] skipProgrammaTeam = config.getEnv().getProperty("skipProgrammaTeam").split(";");
			String[] skipStandTeam = config.getEnv().getProperty("skipStandTeam").split(";");
			for (String team : teams) {
				String[] teamIds = team.split("-");
				if (skipProg || Arrays.asList(skipProgrammaTeam).contains(teamIds[0])) {
					//skip programma
				} else {
				//programma
				dr.get("https://www.voetbal.nl/team/" + teamIds[1] +"/programma");
				Thread.sleep(1000);
				dr.findElement(By.xpath("//span[@class='button--show-more-label']")).click();
				Thread.sleep(2000);
				sourceHtml = dr.getPageSource();
				// open second tab in firefox
				dr.switchTo().window(secondTab.get());
				dr.get(config.getEnv().getProperty("c-baseurl") + "?" +	"teamID=" + teamIds[0] + "&soort=programma");
				dr.findElement(By.xpath("//*[@name='html']"));
				setClipboardContents(sourceHtml);
				dr.findElement(By.xpath("//*[@name='html']")).click();
				dr.findElement(By.xpath("//*[@name='html']")).sendKeys(Keys.CONTROL + "v");
				//dr.findElement(By.xpath("//*[@name='html']")).sendKeys(sourceHtml);
				dr.findElement(By.xpath("//*[@name='Submit']")).click();
				Assert.assertTrue(dr.getPageSource().contains("Programma opgeslagen voor teamID: " +teamIds[0]));
				// switch to first tab
				dr.switchTo().window(firstTab.get());
				System.out.println("Window handle: " + firstTab.get());
				}
				//uitslagen
				if (skipUitslag || Arrays.asList(skipUitslagTeam).contains(teamIds[0])) {
					//skip skipUitslag
				} else {
				dr.get("https://www.voetbal.nl/team/" + teamIds[1] +"/uitslagen");
				Thread.sleep(2000);
				dr.findElement(By.xpath("//span[@class='button--show-more-label']")).click();
				Thread.sleep(2000);
				sourceHtml = dr.getPageSource();
				// open second tab in firefox
				dr.switchTo().window(secondTab.get());
				dr.get(config.getEnv().getProperty("c-baseurl") + "?" +	"teamID=" + teamIds[0] + "&soort=uitslag");
				dr.findElement(By.xpath("//*[@name='html']"));
				setClipboardContents(sourceHtml);
				dr.findElement(By.xpath("//*[@name='html']")).click();
				dr.findElement(By.xpath("//*[@name='html']")).sendKeys(Keys.CONTROL + "v");
				//dr.findElement(By.xpath("//*[@name='html']")).sendKeys(sourceHtml);
				dr.findElement(By.xpath("//*[@name='Submit']")).click();
				Assert.assertTrue(dr.getPageSource().contains("Uitslagen opgeslagen voor teamID: " +teamIds[0]));
				// switch to first tab
				dr.switchTo().window(firstTab.get());
				System.out.println("Window handle: " + firstTab.get());
				}
				if (skipStand || Arrays.asList(skipStandTeam).contains(teamIds[0])) {
					//skip stand
				} else {
				//Stand
				dr.get("https://www.voetbal.nl/team/" + teamIds[1] +"/stand");
				Thread.sleep(2000);
				sourceHtml = dr.getPageSource();
				// open second tab in firefox
				dr.switchTo().window(secondTab.get());
				dr.get(config.getEnv().getProperty("c-baseurl") + "?" +	"teamID=" + teamIds[0] + "&soort=stand");
				dr.findElement(By.xpath("//*[@name='html']"));
				setClipboardContents(sourceHtml);
				dr.findElement(By.xpath("//*[@name='html']")).click();
				dr.findElement(By.xpath("//*[@name='html']")).sendKeys(Keys.CONTROL + "v");
				//dr.findElement(By.xpath("//*[@name='html']")).sendKeys(sourceHtml);
				dr.findElement(By.xpath("//*[@name='Submit']")).click();
				Assert.assertTrue(dr.getPageSource().contains("Standen opgeslagen voor teamID: " +teamIds[0]));
				// switch to first tab
				dr.switchTo().window(firstTab.get());
				System.out.println("Window handle: " + firstTab.get());
				}
			}
		}
	}

	@Then("^voetbalnl page should be displayed$")
	public void verifySuccessful() {
		String expectedText = config.getEnv().getProperty("c-clubnaam").toUpperCase().trim();
		String actualText = dr.findElement(By.xpath("//*[@class='dashboard-header-maintitle']")).getText().toUpperCase()
				.trim();
		assertTrue("Inloggen voetbal.nl gaat fout.", actualText.contains(expectedText));
		// Close the browser
		dr.close();
		dr.quit();
	}

	private void setClipboardContents(String text) {
	  StringSelection stringSelection = new StringSelection( text );
	  Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
	  clipboard.setContents(stringSelection, null);
	}

}
