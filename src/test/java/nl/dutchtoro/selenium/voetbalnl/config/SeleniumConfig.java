package nl.dutchtoro.selenium.voetbalnl.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@ComponentScan(basePackages = { "nl.dutchtoro.selenium.*" })
@PropertySource("classpath:voetbalnl.properties")
public class SeleniumConfig {
   public Environment getEnv() {
	   System.out.println(env.getProperty("email"));
      return env;
   }

   @Autowired
   private Environment env;
}