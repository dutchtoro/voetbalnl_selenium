package nl.dutchtoro.selenium.voetbalnl;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@SuppressWarnings("deprecation")
@RunWith(Cucumber.class)
@CucumberOptions(plugin={"pretty","html:reports/test-report"}, features="src/test/resources")
public class CucumberRunnerTest {
}