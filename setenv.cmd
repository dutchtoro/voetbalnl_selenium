@echo off
rem use for without eclipse, run with: mvn test
set JAVA_HOME=C:\Program Files\Java\jdk1.8.0_171
set M2_HOME=c:\work\tooling\apache-maven-3.5.3
set M2_REPO=C:\Gebruikers\xxxxx\.m2\repository
set MAVEN_OPTS=-Xmx1024m
set _JAVA_OPTIONS=-XX:+ForceTimeHighResolution -Dsun.java2d.noddraw=true -Duser.timezone='Europe/Amsterdam'
set PATH=%JAVA_HOME%\bin;%M2_HOME%\bin;c:\windows\system32;

echo.
ECHO !!!! Your environment has been set.
echo.
ECHO !!!! Java version:
java -fullversion
ECHO !!!! Maven version:
mvn -version

