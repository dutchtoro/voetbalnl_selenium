Voorbereiding:
- Installeer GoogleChrome browser
- Installeer een java jdk (jdk8u171)
- Install Eclipse voor java ontwikkelaars (Eclipse IDE for java developers)
  (als je de windows installer gebruikt, mocht je een foutmelding krijgen, zet je firewall in windows even uit en probeer het nog een keer)
- lokale installatie van maven (zip downloaden en uitpakken in directory, als je zonder eclipse wil draaien, deze stap is niet perse nodig)
- eclipse plugin cucumber (install new software van site: http://cucumber.github.com/cucumber-eclipse/update-site)
- Stel de java-jdk (in eclipse), ga naar Window > Preferences > Java > Installed JREs > and controleer je geinstalleerde JREs. Kies de geinstalleerde Java-JDK. 
- download chromedriver_win32.zip van https://chromedriver.storage.googleapis.com/index.html?path=2.44/ en pak deze zet uit in een bepaalde directory
  info: http://chromedriver.chromium.org/downloads 
- download het selenium voetbalnl project van https://gitlab.com/dutchtoro/voetbalnl_selenium en zet dit in een lokale directory
- importeer het gedownloade project in eclipse met Open Project from file system
- zoek daarna in het project (Package Explorer) naar het property bestand: /srt/test/resources/voetbalnl.proprties en vul de juiste waarden in voor jou site en je account bij voetbal.nl
  Voor de eerste keer zet in je de property login=true (en moet je 1x handmatig inloggen, met verkeersborden e.d., daarna kan de property op false)    
  Als je de data van teams op wil halen zet je de property teamsdata=true anders teamsdata=false,
  de teams stel je dmv teams=<id van voetbalripper>-<code van voetbal.nl>
  voorbeeld: 
  teams=1-T1000099999;2-T1000088888
  De onderstaande url's van voetbal.nl worden daardoor opgevraagd:
  https://www.voetbal.nl/team/T1000099999/programma
  https://www.voetbal.nl/team/T1000099999/uitslagen
  https://www.voetbal.nl/team/T1000099999/stand
  etc...
  Deze worden ingevuld op de clubsite met url's:
  http://www.voetbalclubnaam.nl/vripper/index3.php?teamID=1&soort=programma
  http://www.voetbalclubnaam.nl/vripper/index3.php?teamID=1&soort=uitslag
  http://www.voetbalclubnaam.nl/vripper/index3.php?teamID=1&soort=stand
  etc...
  Je kan het complete team Programma, Uitslag of Stand overslaan door onderstaande properties aan te passen:
  skipProgramma=false
  skipUitslag=false
  skipStand=false
  Je kan per team het Programma, de Uitslag of de Stand overslaan door onderstaande properties aan te passen en het id van het team toe te voegen 
  in een lijst te zetten gesplitst door een ;  (zie onderstaand voorbeeld)
  skipProgrammaTeam=2;3
  skipUitslagTeam=3;4
  skipStandTeam=3;4
Testprogramma uitvoeren:
- daarna als alles goed is gegaan en je geen java-build errors hebt,
  ga je in eclipse Package Explorer op zoek naar /src/test/java/CucumberRunnerTest.java,
  klik met de rechtermuisknop op het bestand en kies Run As Junit Test
- daarna start de test op en gaat het programma googlechrome opstarten en inloggen
  bij voetbal.nl, hij zoekt daarna de clubuitslagen, clubprogramma en teamdata op en gaat deze data vastleggen bij je club-url waar index3.php staat  
  

- Testomgeving
  Windows 10 Prof 64x met geinstalleerde googlechrome
  
